# captcha

```typescript
import Captcha from "captcha/src/Captcha";
import CloudflareTurnstile from "captcha/src/CloudflareTurnstile";
import Recaptcha from "captcha/src/Recaptcha";
import DummyCaptcha from "captcha/src/DummyCaptcha";

let captcha: Captcha;
if (CLOUDFLARE_TURNSTILE_SITE_KEY)
  captcha = new CloudflareTurnstile({ siteKey: CLOUDFLARE_TURNSTILE_SITE_KEY })
else if (RECAPTCHA_SITE_KEY)
  captcha = new Recaptcha({ siteKey: RECAPTCHA_SITE_KEY })
else
  captcha = new DummyCaptcha()

await captcha.getToken();

```
