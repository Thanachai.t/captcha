export default interface Captcha {
  init(): Promise<void>;
  getToken(element?: string | HTMLElement): Promise<string | void>;
  render(element?: string | HTMLElement): Promise<void>;
}