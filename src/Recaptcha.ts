import Captcha from "./Captcha";

export const utils = {
  SiteKeyForTesting: '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI'
}

export enum RecaptchaTheme {
  LIGHT = 'light',
  DARK = 'dark',
  AUTO = 'auto',
}

export default class Recaptcha implements Captcha {
  private isInitialized = false;
  private element!: HTMLElement;
  private token!: string;
  private siteKey: string;
  private options: { theme?: RecaptchaTheme };


  constructor({ siteKey, theme }: { siteKey: string, theme?: RecaptchaTheme }) {
    this.siteKey = siteKey;
    this.options = { theme };
  }

  async init(): Promise<void> {
    if (!this.isInitialized) {
      return new Promise(async (resolve) => {
        const script = document.createElement('script');
        script.src = `https://www.google.com/recaptcha/api.js?render=${this.siteKey}&hl=th`;
        document.head.appendChild(script);

        while (!window['Recaptcha']) await new Promise(r => setTimeout(r, 10))

        const badge: HTMLElement = document.querySelector('.Recaptcha-badge');

        // if (badge)
        //   badge.style.visibility = 'hidden';

        window['Recaptcha'].ready(() => {
          this.isInitialized = true
          resolve()
        })
      })
    }
  }

  async getToken(element?: string | HTMLElement): Promise<string> {
    if (!this.token)
      await this.init();
    return await window['Recaptcha'].execute(this.siteKey, { action: 'submit' });
  }
  async render(element?: string | HTMLElement): Promise<void> {
    await this.init()

    if (element) {
      if (typeof element === 'string') {
        this.element = document.querySelector(element);
      } else {
        this.element = element;
      }
    } else {
      const div = document.createElement('div');
      div.style.position = 'fixed';
      div.style.top = '-500px';

      document.body.appendChild(div);
      this.element = div;
    }

    const turnstile = window['turnstile'];

    return new Promise((resolve) => {
      turnstile.render(this.element, {
        sitekey: this.siteKey,
        ...this.options,
        callback: (token) => {
          this.token = token
          resolve()
        }
      });
    })
  }
}
