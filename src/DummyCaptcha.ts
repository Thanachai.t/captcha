import Captcha from "./Captcha";

class DummyCaptcha implements Captcha {
  async init(): Promise<void> { }
  async getToken(element?: string | HTMLElement): Promise<string | void> { }
  async render(element?: string | HTMLElement): Promise<void> { }
}

export default DummyCaptcha;
