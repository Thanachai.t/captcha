import Captcha from "./Captcha";

enum SiteKeyForTesting {
  ALWAYS_PASSES = '1x00000000000000000000AA',
  ALWAYS_BLOCKS = '2x00000000000000000000AB',
  FORCES_AN_INTERACTIVE_CHALLENGE = '3x00000000000000000000FF',
}

export const utils = {
  SiteKeyForTesting
}

export enum CloudflareTurnstileTheme {
  LIGHT = 'light',
  DARK = 'dark',
  AUTO = 'auto',
}

export default class CloudflareTurnstile implements Captcha {
  private isInitialized = false;
  private element!: HTMLElement;
  private token!: string;
  private siteKey: string | SiteKeyForTesting;
  private options: { theme?: CloudflareTurnstileTheme };


  constructor({ siteKey, theme }: { siteKey: string | SiteKeyForTesting, theme?: CloudflareTurnstileTheme }) {
    this.siteKey = siteKey;
    this.options = { theme };
  }

  async init(): Promise<void> {
    if (!this.isInitialized) {
      return new Promise(async (resolve) => {
        const script = document.createElement('script');
        script.src = 'https://challenges.cloudflare.com/turnstile/v0/api.js?render=explicit';
        script.async = true;
        script.defer = true;

        document.head.appendChild(script);

        while (!window['turnstile']) await new Promise(r => setTimeout(r, 10))
        this.isInitialized = true
        resolve(window['turnstile'])
      })
    }
  }
  async getToken(element?: string | HTMLElement): Promise<string> {
    if (!this.token)
      await this.render(element);

    const token = this.token;
    this.token = null;

    return token;
  }
  async render(element?: string | HTMLElement): Promise<void> {
    await this.init()

    if (element) {
      if (typeof element === 'string') {
        this.element = document.querySelector(element);
      } else {
        this.element = element;
      }
    } else {
      const div = document.createElement('div');
      div.style.position = 'fixed';
      div.style.top = '-500px';

      document.body.appendChild(div);
      this.element = div;
    }

    const turnstile = window['turnstile'];

    return new Promise((resolve) => {
      turnstile.render(this.element, {
        sitekey: this.siteKey,
        ...this.options,
        callback: (token) => {
          this.token = token
          resolve()
        },
        'expired-callback': () => {
        },
        'timeout-callback': () => {
        },
        'error-callback': () => {
          throw new Error('network error');
        }
      });
    })
  }
}
